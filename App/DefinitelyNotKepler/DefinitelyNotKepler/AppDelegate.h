//
//  AppDelegate.h
//  DefinitelyNotKepler
//
//  Created by Dirk Zimmermann on 19/6/24.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
