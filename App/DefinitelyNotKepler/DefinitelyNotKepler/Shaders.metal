//
//  Shaders.metal
//  DefinitelyNotKepler
//
//  Created by Dirk Zimmermann on 19/6/24.
//

// File for Metal kernel and shader functions

#include <metal_stdlib>
#include <simd/simd.h>

// Including header shared between this Metal shader code and Swift/C code executing Metal API
// commands
#import "ShaderTypes.hpp"

using namespace metal;

typedef struct
{
    float3 position [[attribute(VertexAttributePosition)]];
    float2 texCoord [[attribute(VertexAttributeTexcoord)]];
    float3 normal [[attribute(VertexAttributeNormal)]];
} Vertex;

typedef struct
{
    float4 position [[position]];
    float4 normal;
    float2 texCoord;
} ColorInOut;

vertex ColorInOut vertexShader(Vertex in [[stage_in]],
                               constant Uniforms &uniforms [[buffer(BufferIndexUniforms)]])
{
    ColorInOut out;

    float4 position = float4(in.position, 1.0);
    float4 normal = float4(in.normal, 1);
    out.position = uniforms.projectionMatrix * uniforms.modelViewMatrix * position;
    out.normal = normalize(uniforms.normalTransformationMatrix * normal);
    out.texCoord = in.texCoord;

    return out;
}

fragment float4 fragmentShader(ColorInOut in [[stage_in]],
                               constant Uniforms &uniforms [[buffer(BufferIndexUniforms)]],
                               texture2d<float> colorMap [[texture(TextureIndexColor)]])
{
    constexpr sampler colorSampler(mip_filter::linear, mag_filter::linear, min_filter::linear);

    float4 colorSample = colorMap.sample(colorSampler, in.texCoord.xy);

    return colorSample;
}
