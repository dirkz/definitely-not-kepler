//
//  ShaderTypes.h
//  DefinitelyNotKepler
//
//  Created by Dirk Zimmermann on 19/6/24.
//

//
//  Header containing types and enum constants shared between Metal shaders and Swift/ObjC source
//
#ifndef ShaderTypes_h
#define ShaderTypes_h

#ifdef __METAL_VERSION__
#define NS_ENUM(_type, _name)                                                                      \
    enum _name : _type _name;                                                                      \
    enum _name : _type
typedef metal::int32_t EnumBackingType;
#else
#import <Foundation/Foundation.h>
typedef NSInteger EnumBackingType;
#endif

#include <simd/simd.h>

using namespace simd;

typedef NS_ENUM(EnumBackingType, BufferIndex) {
    BufferIndexPositions = 0,
    BufferIndexTexCoords,
    BufferIndexNormal,
    BufferIndexUniforms
};

typedef NS_ENUM(EnumBackingType, VertexAttribute) {
    VertexAttributePosition = 0,
    VertexAttributeTexcoord,
    VertexAttributeNormal,
};

typedef NS_ENUM(EnumBackingType, TextureIndex) {
    TextureIndexColor = 0,
};

typedef struct
{
    float4x4 projectionMatrix;
    float4x4 modelViewMatrix;
    float4x4 normalTransformationMatrix;
} Uniforms;

#endif /* ShaderTypes_h */
