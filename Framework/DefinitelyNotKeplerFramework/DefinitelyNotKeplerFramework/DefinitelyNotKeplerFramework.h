//
//  DefinitelyNotKeplerFramework.h
//  DefinitelyNotKeplerFramework
//
//  Created by Dirk Zimmermann on 19/6/24.
//

#import <Foundation/Foundation.h>

//! Project version number for DefinitelyNotKeplerFramework.
FOUNDATION_EXPORT double DefinitelyNotKeplerFrameworkVersionNumber;

//! Project version string for DefinitelyNotKeplerFramework.
FOUNDATION_EXPORT const unsigned char DefinitelyNotKeplerFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DefinitelyNotKeplerFramework/PublicHeader.h>

#include <DefinitelyNotKeplerFramework/Perlin.hpp>
