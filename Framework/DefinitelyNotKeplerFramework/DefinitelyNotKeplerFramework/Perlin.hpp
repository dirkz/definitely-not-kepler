//
//  Perlin.hpp
//  DefinitelyNotKeplerFramework
//
//  Created by Dirk Zimmermann on 19/6/24.
//

#ifndef Perlin_hpp
#define Perlin_hpp

#include <simd/simd.h>

namespace zdnk
{

const int PerlinNoise2DGrid = 255;

struct PerlinNoise2D
{
    PerlinNoise2D();
    float noise(float x, float y);

  private:
    simd::float2 gradients[PerlinNoise2DGrid][PerlinNoise2DGrid + 2];
};

} // namespace zdnk

#endif /* Perlin_hpp */
