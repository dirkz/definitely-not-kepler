//
//  Perlin.cpp
//  DefinitelyNotKeplerFramework
//
//  Created by Dirk Zimmermann on 19/6/24.
//

#include "Perlin.hpp"

#include <random>

using namespace simd;
using namespace std;

namespace zdnk
{

static inline float random_float()
{
    static std::uniform_real_distribution<float> distribution(-1.0, 1.0);
    static std::mt19937 generator;
    return distribution(generator);
}

static inline float smoother_step(float x)
{
    return x * x * x * (x * (6.0f * x - 15.0f) + 10.0f);
}

PerlinNoise2D::PerlinNoise2D()
{
    for (int i = 0; i < PerlinNoise2DGrid + 2; ++i)
    {
        for (int j = 0; j < PerlinNoise2DGrid + 2; ++j)
        {
            gradients[i][j] = normalize(float2{random_float(), random_float()});
        }
    }
}

float PerlinNoise2D::noise(float x, float y)
{
    float floorX = floor(x);
    float floorY = floor(y);

    int gridX = static_cast<int>(floorX) & PerlinNoise2DGrid;
    int gridY = static_cast<int>(floorY) & PerlinNoise2DGrid;

    float2 g00 = gradients[gridX][gridY];
    float2 g10 = gradients[gridX + 1][gridY];
    float2 g01 = gradients[gridX][gridY + 1];
    float2 g11 = gradients[gridX + 1][gridY + 1];

    float2 inside{x - floorX, y - floorY};
    float2 smoothed{smoother_step(inside.x), smoother_step(inside.y)};

    return 0;
}

}; // namespace zdnk
