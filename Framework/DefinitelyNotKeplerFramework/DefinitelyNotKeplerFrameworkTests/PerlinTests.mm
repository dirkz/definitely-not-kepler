//
//  PerlinTests.mm
//  DefinitelyNotKeplerFrameworkTests
//
//  Created by Dirk Zimmermann on 19/6/24.
//

#import <XCTest/XCTest.h>

#import <DefinitelyNotKeplerFramework/DefinitelyNotKeplerFramework.h>

using namespace zdnk;

@interface PerlinTests : XCTestCase

@end

@implementation PerlinTests

- (void)testLimits {
    int a = static_cast<int>(floor(256.5));
    XCTAssertEqual(256, a);
    a &= 255;
    XCTAssertEqual(0, a);

    a = static_cast<int>(floor(255.5));
    XCTAssertEqual(255, a);
    a &= 255;
    XCTAssertEqual(255, a);

    a = static_cast<int>(floor(0));
    XCTAssertEqual(0, a);
    a &= 255;
    XCTAssertEqual(0, a);

    a = static_cast<int>(floor(-1.5));
    XCTAssertEqual(-2, a);
    a &= 255;
    XCTAssertEqual(254, a);

    a = static_cast<int>(floor(-1));
    XCTAssertEqual(-1, a);
    a &= 255;
    XCTAssertEqual(255, a);

    a = static_cast<int>(floor(-255.5));
    XCTAssertEqual(-256, a);
    a &= 255;
    XCTAssertEqual(0, a);

    PerlinNoise2D perlin = PerlinNoise2D();
    perlin.noise(256.6, -3.1);
    perlin.noise(256.0, -3.0);
    perlin.noise(866.8, -1024.2);
}

@end
