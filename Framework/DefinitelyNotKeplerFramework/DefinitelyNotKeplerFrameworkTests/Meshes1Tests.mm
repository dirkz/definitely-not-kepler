//
//  Meshes1Tests.mm
//  DefinitelyNotKeplerFrameworkTests
//
//  Created by Dirk Zimmermann on 19/6/24.
//

#import <XCTest/XCTest.h>

#import <MetalKit/MetalKit.h>

using namespace simd;

@interface Meshes1Tests : XCTestCase

@end

@implementation Meshes1Tests

- (void)testBasicMeshProperties {
    const NSUInteger VertexAttributePosition = 0;
    const NSUInteger VertexAttributeTexCoords = 1;
    const NSUInteger VertexAttributeNormal = 2;

    const NSUInteger BufferIndexPosition = 0;
    const NSUInteger BufferIndexTexCoords = 1;
    const NSUInteger BufferIndexNormal = 2;

    MTLVertexDescriptor *mtlVertexDescriptor = [[MTLVertexDescriptor alloc] init];

    mtlVertexDescriptor.attributes[VertexAttributePosition].format = MTLVertexFormatFloat3;
    mtlVertexDescriptor.attributes[VertexAttributePosition].offset = 0;
    mtlVertexDescriptor.attributes[VertexAttributePosition].bufferIndex = BufferIndexPosition;

    mtlVertexDescriptor.attributes[VertexAttributeTexCoords].format = MTLVertexFormatFloat2;
    mtlVertexDescriptor.attributes[VertexAttributeTexCoords].offset = 0;
    mtlVertexDescriptor.attributes[VertexAttributeTexCoords].bufferIndex = BufferIndexTexCoords;

    mtlVertexDescriptor.attributes[VertexAttributePosition].format = MTLVertexFormatFloat2;
    mtlVertexDescriptor.attributes[VertexAttributePosition].offset = 0;
    mtlVertexDescriptor.attributes[VertexAttributePosition].bufferIndex = BufferIndexNormal;

    mtlVertexDescriptor.layouts[BufferIndexPosition].stride = sizeof(float3);
    mtlVertexDescriptor.layouts[BufferIndexPosition].stepRate = 1;
    mtlVertexDescriptor.layouts[BufferIndexPosition].stepFunction = MTLVertexStepFunctionPerVertex;

    mtlVertexDescriptor.layouts[BufferIndexTexCoords].stride = sizeof(float2);
    mtlVertexDescriptor.layouts[BufferIndexTexCoords].stepRate = 1;
    mtlVertexDescriptor.layouts[BufferIndexTexCoords].stepFunction = MTLVertexStepFunctionPerVertex;

    mtlVertexDescriptor.layouts[BufferIndexNormal].stride = sizeof(float3);
    mtlVertexDescriptor.layouts[BufferIndexNormal].stepRate = 1;
    mtlVertexDescriptor.layouts[BufferIndexNormal].stepFunction = MTLVertexStepFunctionPerVertex;

    MDLVertexDescriptor *mdlVertexDescriptor =
    MTKModelIOVertexDescriptorFromMetal(mtlVertexDescriptor);

    mdlVertexDescriptor.attributes[VertexAttributePosition].name  = MDLVertexAttributePosition;
    mdlVertexDescriptor.attributes[VertexAttributeTexCoords].name  = MDLVertexAttributeTextureCoordinate;
    mdlVertexDescriptor.attributes[VertexAttributeNormal].name  = MDLVertexAttributeNormal;

    MDLMesh *mesh = [MDLMesh newEllipsoidWithRadii:float3{1, 1, 1}
                                    radialSegments:5
                                  verticalSegments:5
                                      geometryType:MDLGeometryTypeTriangles
                                     inwardNormals:NO
                                        hemisphere:NO
                                         allocator:nil];

    mesh.vertexDescriptor = mdlVertexDescriptor;

    for (MDLVertexAttribute *attribute in mesh.vertexDescriptor.attributes) {
        if ([attribute.name length] > 0) {
            NSLog(@"Vertex attribute %@", attribute.name);
        }
    }

    NSArray *submeshes = mesh.submeshes;
    for (MDLSubmesh *submesh in submeshes) {
        NSLog(@"Submesh with %lu indices of bitsize %lu",
              (unsigned long) submesh.indexCount, (unsigned long) submesh.indexType);

        id<MDLMeshBuffer> indexBuffer32 = [submesh indexBufferAsIndexType:MDLIndexBitDepthUint32];
        void *pVoid32 = indexBuffer32.map.bytes;
        void *pVoid32_2 = indexBuffer32.map.bytes;
        XCTAssertEqual(pVoid32, pVoid32_2);

        id<MDLMeshBuffer> indexBuffer16 = [submesh indexBufferAsIndexType:MDLIndexBitDepthUint16];
        void *pVoid16 = indexBuffer16.map.bytes;
        XCTAssertNotEqual(pVoid16, pVoid32);
    }
}

@end
