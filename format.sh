#!/bin/env zsh

find . \( -name \*.cpp -o -name \*.hpp -o -name \*.metal \) -exec clang-format-mp-18 -i {} \;
